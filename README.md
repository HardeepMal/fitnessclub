# README #


### Introduction ###

* This website developed for fitness clubs where users can have accounts and attend online session for their fitness. Due to this covid 19 pandemic, fitness industries and their users are facing problems so this solution can help them. 
* Simple website developed to help users for maintaining their health through online sessions from experts of gyms with less or without any equipment. 

### Technology ###

* HTML
* CSS
* JavaScript
* JQuery

### Required Software ###

* Code Editor (Notepad, Notepad++, VS Code any)
* Web Browser (IE, Google Chrome or Firefox)

### License ###

Copyright 2020 @ Hardeep Kaur. This website is provided as a public resource Free to Use, Refer without any concerns. This can be a starting point for users to build their own websites or its open for improvements. NOBODAY CAN BE LIABLE FOR ANY CLAIMS, ACTION OF CONTRACT FOR USING THIS WEBSITE. 